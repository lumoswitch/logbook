'use strict';

function actionsAtRegister() {
    closeForm();
    var userInputValues = {
        "name": userInput.name.value,
        "surname": userInput.surname.value,
        "username": userInput.username.value,
        "avatar": $('input[name="tab"]:checked').val(),
        "email": userInput.email.value,
        "password": userInput.password.value,
        "rpt_password": userInput.rpt_password.value,
        "gender": $('input[name="gender"]:checked').val(),
        "dateOfBirth": userInput.dateOfBirth.value,
        "country": userInput.country.value,
        "city": userInput.city.value,
        "address": userInput.address.value,
        "job": userInput.job.value,
        "interests": userInput.interests.value,
        "info": userInput.info.value,
    };
    var myJSON = JSON.stringify(userInputValues);
    console.log(myJSON);
    ajaxRequest("POST", "/logbook/register", responseFromRegisterServlet, myJSON);
}



function loginForm() {
    
    var loginInputValues = {
        "username_log" : document.getElementById('username_log').value,
        "password_log" : document.getElementById('pwd_log').value
    };

    var myJSON = JSON.stringify(loginInputValues);
    console.log(myJSON);
    ajaxRequest("POST", "/logbook/login", responseFromLoginServlet, myJSON);
    
}

function editForm(){
    var userInputValues = {
        "name": userInput.name.value,
        "surname": userInput.surname.value,
        "username": userInput.username.value,
        "avatar": $('input[name="tab"]:checked').val(),
        "email": userInput.email.value,
        "password": userInput.password.value,
        "rpt_password": userInput.rpt_password.value,
        "gender": $('input[name="gender"]:checked').val(),
        "dateOfBirth": userInput.dateOfBirth.value,
        "country": userInput.country.value,
        "city": userInput.city.value,
        "address": userInput.address.value,
        "job": userInput.job.value,
        "interests": userInput.interests.value,
        "info": userInput.info.value,
    };
    var myJSON = JSON.stringify(userInputValues);
    ajaxRequest("POST", "/logbook/editUser", responseFromEditServlet, myJSON);
    document.getElementById("saveB").style.display ="none";
    document.getElementById("cancel").style.display ="none";
    document.getElementById("close").style.display ="block";
}

function showProfile(){
    document.getElementById("usersInPlatform").style.display ="none";
    document.getElementById("alert_msg").style.display ="none";
    editForm();
    closeNav();
}

function logout(){
    var logged_user = {
        "username": document.getElementById("logged-in-name").value
    };
    var myJSON = JSON.stringify(logged_user);
    ajaxRequest("POST", "/logbook/logout", responseFromLogoutServlet, myJSON);
}

