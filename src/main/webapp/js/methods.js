'use strict';

document.getElementById('vidcanv').style.display ='none';
document.getElementById('photoBooth').style.display ='none';
document.getElementById("user-menu").style.display = "none";
document.getElementById("page-after-logging").style.display = "none";
document.getElementById('userProfile').style.display = "none";
document.getElementById('logged-in-menu').style.display = "none";
document.getElementById("saveB").style.display ="none";
document.getElementById("close").style.display ="none";
document.getElementById("usersInPlatform").style.display ="none";
document.getElementById("delete").style.display ="none";
document.getElementById("usersTable").style.display ="none";

/*Function: ajaxRequest
* Parameters: method,uri,handler
* Details: ajaxRequest makes all the requests to the server 
           On readyState == 4 a function is called.
*/
function ajaxRequest(method,uri,handler,inputs){

    var xhttp = new XMLHttpRequest();
    
    xhttp.onreadystatechange = function () { if(xhttp.readyState==4){ handler(xhttp); }};    
    xhttp.open(method, uri, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(inputs);

}

/*Function: hide
* Parameters: none
* Details: hides take photo and upload image buttons
*/
function hide(){
    document.getElementById('vidcanv').style.display ='none';
}

/*Function: show
* Parameters: none
* Details: shows take photo and upload image buttons and initialize faceAPI
*/
function show(elem){
    faceRec.init();
    document.getElementById(elem).style.display = 'block';
    
}

function closeForm(){
    $('#register').modal('hide');
}

function closeLoginForm(){
    $('#login').modal('hide');
}

function openForm(){
    $('#register').modal('show');
}

$(document).ready(function () {   
    ajaxRequest("POST", "/logbook/sessionServlet", responseFromSessionServlet, null);  
});

